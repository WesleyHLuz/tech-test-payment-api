using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;
        private Vendedor vendedor;
        private Cadastro _cadastro;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult AdicionarVenda(Venda venda)
        {
            var vendedor = _context.Vendedores.Find(venda.IdVendedor);
            if (vendedor == null) return NotFound("Vendedor não encontrado");
            if (string.IsNullOrEmpty(venda.Itens)) return UnprocessableEntity("Venda sem item");
            venda.StatusVenda = "Aguardando Pagamento";
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpGet("")]
        public IActionResult ObterTodasVendas()
        {
            var vendas = _context.Vendas.ToList();
            Cadastro cadastro;

            List<Cadastro> payments = new List<Cadastro>();

            foreach (var venda in vendas)
            {
                cadastro = new Cadastro();
                cadastro.AdicionaVenda(venda);
                cadastro.AdicionaVendedor(Validador.GetVendedor(venda.IdVendedor, _context));
                payments.Add(cadastro);
            }

            return Ok(payments);
        }

        

        [HttpGet("ObterVendaId/{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null) return NotFound();
            Cadastro cadastro;
            cadastro = new Cadastro();
            cadastro.AdicionaVenda(venda);
            cadastro.AdicionaVendedor(Validador.GetVendedor(venda.IdVendedor, _context));
            return Ok(cadastro);
        }

        [HttpPut("AtualizarStatus/{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            if (venda == null) return NotFound();
            if (Validador.PodeAtualizar(venda, vendaBanco))
            {
                vendaBanco.StatusVenda = venda.StatusVenda;
                _context.SaveChanges();
                return Ok(vendaBanco);
            }
            if (vendaBanco.StatusVenda == "Entregue") return UnprocessableEntity("A venda já foi entregue");
            if (vendaBanco.StatusVenda == "Cancelada") return UnprocessableEntity("A venda foi cancelada");
            return UnprocessableEntity("Status não permitido");
        }
    }

    public static class Validador
    {
        public static Vendedor GetVendedor(int id, VendaContext vendaContext)
        {
            var vendedor = vendaContext.Vendedores.Find(id);
            return vendedor;
        }

        public static bool PodeAtualizar(Venda venda, Venda vendaBanco)
        {
            if (vendaBanco.StatusVenda.Equals("Aguardando Pagamento"))
            {
                if (venda.StatusVenda.Equals("Pagamento Aprovado") || venda.StatusVenda.Equals("Cancelada"))
                {
                    return true;
                }
            }
            if (vendaBanco.StatusVenda.Equals("Pagamento Aprovado"))
            {
                if (venda.StatusVenda.Equals("Enviado para Transportadora") || venda.StatusVenda.Equals("Cancelada"))
                {
                    return true;
                }
            }
            if (vendaBanco.StatusVenda.Equals("Enviado para Transportadora"))
            {
                if (venda.StatusVenda.Equals("Entregue"))
                {
                    return true;
                }
            }
            return false;
        }
    }
}